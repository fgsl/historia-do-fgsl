# HISTÓRIA DOS FÓRUNS GOIANOS DE SOFTWARE LIVRE (FGSL)
* Este projeto de história está em eterna construção e disponível no projeto do Gitlab:
  * https://gitlab.com/fgsl/historia-do-fgsl
* Caro desejar colaborar, é só pedir para entrar no projeto acima ;-)

# 2004 - FGSL1
* Dias: 25/11/04 (quinta-feira) a 27/11/04 (sábado)
* Locais: SENAC, UFG, UCG, Universidade Salgado Filho, SENAR/FAEG
* Participaram da organização:
  * Cláudio Martins Garcia
  * Clenio W Silva
  * Danielle Gomes de Oliveira
  * Junio José
  * Marcelo Akira Inuzuka
* Inscrições:
  * Valores: gratuito
  * Participantes: não contabilizado, estima-se em 200 no total
  * Certificados: não foi emitido
* Palestrantes convidados:
  * Gustavo Noronha
  * Marcelo Branco
  * Pedro Rezende
* Observações: 
  * Realizado em vários locais, com minicursos itinerantes (SENAC, UFG, UCG e Salgado)
  * No dia 26/11 (sexta-feira), com a palestra de Marcelo Branco, foi realizado a fundação do Projeto Software Livre Goiás (PSL-GO)
  * A fundação do PSL-GO se deu com leitura do manifesto
  * No sábado 27/11 foi realizado palestras com Pedro Rezende e Gustavo Noronha no SENAR/FAEG
* Referências:
  * wiki.softwarelivre.org/PSLGO (fora do ar desde 2018)
  * https://web.archive.org/web/20100915022028/http://wiki.softwarelivre.org:80/PSLGO/AtasDeReunioes
  * https://web.archive.org/web/20100915021730/http://wiki.softwarelivre.org:80/PSLGO/MaterialDeDivulgacao

# 2005 - FGSL2
* Dias: 01/10/05 (sábado)
* Local: Faculdade Anhanguera - Goiânia-GO
* Participaram da organização:
  * Cláudio Martins Garcia
  * Clenio W Silva
  * Danielle Gomes de Oliveira
  * Eduardo 
  * Frederico R. C. Costa
  * Junio José
  * Marcelo Akira Inuzuka
  * Yves Junqueira
  * Outros: ?
* Participantes convidados:
  * César Brod
  * Piter Punk 
* Inscrições:
  * Valores: ?
  * Certificados: emitidos, quantidade?
* Observações:
  * Primeiro evento com camiseta, cor vermelha
  * Primeiro evento com chamada de trabalhos
  * Primeira emissão de certificados em PDF
* Referências:
  * https://groups.google.com/forum/#!forum/fgsl

# 2006 - FGSL3
* Dias: 27/10/06 (sexta-feira) a 28/10/06 (Sábado)
* Local: Faculdades Alves Faria - Goiânia-GO
* Participaram da organização:
  * ?
* Participantes convidados:
  * Sérgio Amadeu da Silveira
  * Mauro Carvalho Chehab - kernel.org
* Inscrições:
  * Valores: ?
  * Certificados: emitidos, quantidade?
* Observações:
  * Primeiro evento com abertura na sexta-feira à noite
* Referências:
  * https://groups.google.com/forum/#!forum/fgsl

# 2007 - FGSL4
* Dias: 29/10/07 (segunda-feira) a 31/10/07 (quarta-feira)
* Local: Oliveira's Place - Goiânia-GO
* Participaram da organização:
  * Junio José
* Participantes convidados:
  * Não houve
* Inscrições:
  * Valores: gratuito
  * Certificados: não houve
* Observações:
  * Inicialmente era para ser realizado no SENAC Cora Coralina
  * Realizado como sub-evento em parceria com outro evento voltado para negócios em TI
* Referências:
  * https://groups.google.com/forum/#!forum/fgsl

# 2008 - FGSL5
* Dias: 30/05/08 (sexta-feira) a 31/05/08 (sábado)
* Local: SENAI/FATESG do Setor Universitário - Goiânia-GO
* Participaram da organização:
  * Junio José
* Participantes convidados:
  * Sérgio Amadeu da Silveira (abertura)
* Inscrições:
  * Valores: gratuito
  * Certificados: ? quantidade
* Observações:
  * Contou com a presença do governador
  * Realizado em parceria com a UEG (Evento SGSL)
* Referências:
  * http://fgsl.aslgo.org.br:80/fgsl5/

# 2009 - FGSL6
* Dias: 02/10/09 (sexta-feira) a 03/10/09 (sábado)
* Local: SENAC Cora Coralina - Goiânia-GO
* Participaram da organização:
  * ?
* Participantes convidados:
  * Paulino Michelazzo (abertura)
* Inscrições:
  * Valores: gratuito
  * Certificados: ? quantidade
* Observações:
  * ?
* Referências:
  * ?

# 2010 - FGSL7
* Dias: 10/12/10 (sexta-feira) a 11/12/10 (sábado)
* Local: Faculdade Cambury - Goiânia-GO
* Participaram da organização:
  * Joelias Júnior
* Participantes convidados:
  * ? (abertura)
* Inscrições:
  * Valores: gratuito
  * Certificados: ? quantidade
* Observações:
  * ?
  * ?
* Referências:
  * ?

# 2011 - FGSL8
* Dias: 10/12/11 (sexta-feira) a 11/12/11 (sábado)
* Local: SENAC Cora Coralina - Goiânia-GO
* Participaram da organização:
  * Andressa Martins (catoze): coordenadora
  * Welma Alves: coordenadora
  * Coordenação de Patrocínio:Aliny Meire
  * Coordenação de Divulgação: Georjuan Taylor
  * Coordenação de Caravanas: Georjuan Taylor
  * Coordenação de Desenvolvimento: Hugo Seabra
  * Coordenação de Infra-Estrutura: Sebastião dos Santos
  * Coordenação do Temário: Andressa Martins
  * Coordenação de Inscrições: Hugo Seabra
  * Coordenação de Credenciamento: Aliny Meire
  * Batismo digital: Wendell Bento 
* Participantes convidados:
  * ? (abertura)
* Inscrições:
  * Valores: gratuito
  * Certificados: ? quantidade
* Observações:
  * ?
  * ?
* Referências:
  * https://groups.google.com/forum/#!forum/fgsl

# 2012 - FGSL9
* Dias: 30/11/12 (sexta-feira) a 01/12/08 (sábado)
* Local: IFG Goiânia - Goiânia-GO
* Participaram da organização:
  * Fábio Marques: coordenador geral
  * Joelias Júnior: coordenador geral
  * Outros: ?
* Participantes convidados:
  * Richard Stallman (abertura)
* Inscrições:
  * Valores: gratuito
  * Certificados: ? quantidade
* Observações:
  * ?
* Referências:
  * https://groups.google.com/forum/#!forum/fgsl

# 2013 - FGSL10
* Dias: 29/11/13 (sexta-feira) a 30/11/13 (sábado)
* Local: Senac Cora Coralina - Goiânia-GO
* Participaram da organização:
  * ?
* Participantes convidados:
  * Pedro Rezende (abertura)
* Inscrições:
  * Valores: ?
  * Certificados: ? quantidade
* Observações:
  * Primeiro FGSL em parceria com a Escola Regional de Goiás (I ERIGO)
  * ?
* Referências:
  * https://groups.google.com/forum/#!forum/fgsl

# 2014 - FGSL11
* Dias: 21/11/14 (sexta-feira) a 22/11/14 (sábado)
* Local: UFG - Centro de Aulas D - Goiânia-GO
* Participaram da organização:
  * ?
* Participantes convidados:
  * Jon 'Madog' Hall (abertura)
* Inscrições:
  * Valores: ?
  * Certificados: ? quantidade
* Observações:
  * Realizado em parceria com a II Escola Regional de Goiás (II ERIGO)
  * Boa parte da interação ocorreu via Telegram ou Google Hangout
* Referências:
  * https://groups.google.com/forum/#!forum/fgsl

# 2015 - FGSL12
* Dias: 04/12/15 (sexta-feira) a 05/12/15 (sábado)
* Local: SENAC Cora Coralina - Goiânia-GO
* Participaram da organização:
  * Coord. Geral: Marcelo akira Inuzuka
  * Vice coord. geral: Jean Pierre B. Sousa
  * outros: 
* Participantes convidados:
  * Jon 'Madog' Hall (abertura)
* Inscrições:
  * Valores: ?
  * Certificados: ? quantidade
* Observações:
  * Realizado em parceria com a III Escola Regional de Goiás (III ERIGO)
  * Boa parte da interação ocorreu via Telegram ou Google Hangout
  * Estreia do domínio http://2015.fgsl.net
  * Estreia do software Weby como gerenciador de conteúdo
  * Certificados disponíveis em: http://certificados.fgsl.net
* Referências:
  * https://groups.google.com/forum/#!forum/fgsl
  * http://2015.fgsl.net/ (histórico preservado)
  * https://gitlab.com/fgsl/documentacao-do-fgsl/wikis/home (início de documentação)
  * https://gitlab.com/fgsl/fgsl-artes (início de documentação)


# 2016 - FGSL13
* Dias: 16/11/16 (quarta-feira) a 19/11/16 (sábado)
* Local: UFG Campus Sambaia - Goiânia-GO
* Participaram da organização:
  * Coord. Geral: Ole Peter Smith
  * outros: ?
* Participantes convidados:
  * ?
* Inscrições:
  * Valores: ?
  * Certificados: ? quantidade
* Observações:
  * Realizado em parceria com a IV Escola Regional de Goiás (IV ERIGO)
  * Realizado em parceria com o XXX ENECOMP
  * Boa parte da interação ocorreu via Telegram ou Google Hangout
  * Certificados disponíveis em: http://certificados.fgsl.net
* Referências:
  * https://groups.google.com/forum/#!forum/fgsl
  * http://2016.fgsl.net/ (histórico preservado)

# 2017 - FGSL14
* Dias: 16/11/17 (quinta-feira) a 18/11/17 (sábado)
* Local: UFG Centro de Aulas E - Goiânia-GO
* Participaram da organização:
  * Coord. Geral: Marcelo Akira Inuzuka
  * outros: ?
* Participantes convidados:
  * ?
* Inscrições:
  * Valores: ?
  * Certificados: 
    * Colaborador: 33
    * Participante: 176
    * Organização: 12
    * Palestrante: 57
    * Total: 342
* Atividades (palestras, oficinais e minicursos): 52
* Observações:
  * Realizado em parceria com a V Escola Regional de Goiás (V ERIGO)
  * Realizado em parceria com o I Escola Regional de Sistemas de Informação (I ERSIGO)
  * Boa parte da interação ocorreu via Telegram ou Google Hangout
  * Certificados disponíveis em: http://certificados.fgsl.net
* Referências:
  * https://groups.google.com/forum/#!forum/fgsl
  * http://2017.fgsl.net/ (histórico preservado)

# 2018 - FGSL15
* Dias: 22/11/17 (quinta-feira) a 24/11/17 (sábado)
* Local: UFG Centro de Aulas E - Goiânia-GO
* Participaram da organização:
  * Coord. Geral: Marcelo Akira Inuzuka
  * outros: ?
* Participantes convidados:
  * ?
* Inscrições:
  * Valores: gratuito
  * Certificados: ? quantidade
* Observações:
  * Fim da parceria com a Escola Regional de Goiás (ERIGO)
  * Boa parte da interação ocorreu via Riot.im
  * Certificados disponíveis em: ?
* Referências:
  * https://groups.google.com/forum/#!forum/fgsl
  * http://2018.fgsl.net/ (histórico preservado)
